package rotatematrix;

import static java.lang.System.out; 

/**
 * This class provides helper method to rotate any NxN matrix in the multiple of +90 or -90 degrees
 * 
 * @author Dhananjay
 *
 */
public class RotateMatrix<T> {
	
	/**
	 * Prints the matrix.
	 *
	 * @param <T> the generic type
	 * @param m the matrix
	 */
	private static <T> void printMatrix(T[][] m){
	  for (int i = 0; i < m.length; i++){
	    for (int j = 0; j < m.length; j++){
	    out.printf("%-10s", m[i][j].toString());
	    }
	    out.println();
	  }
	}

	
	/**
	 * Gets the rotation count.
	 *
	 * @param angle the angle
	 * @return the rotation cnt
	 */
	private static Integer getRotationCnt(int angle){
		int rotateCnt = angle / 90;
		int remainder = angle % 90;
		
		if (remainder != 0){
			return null;
		}	
		return rotateCnt;
	}
	
	/**
	 * Rotate the matrix in-line in the multiple of 90 degrees either clockwise or anti-clockwise
	 *
	 * @param <T> the generic type
	 * @param angle the angle
	 *        angle must be a positive or negative multiple of 90.
	 * @param matrix the matrix
	 *        matrix must be a N X N matrix. If rows and columns are not equal, then rotation is not performed.
	 * @return the int 
	 *    0    for success 
	 *    != 0 for failure
	 */
	public static <T> int rotate(int angle, T[][] matrix){
	  
	  // check if matrix is null
	  if (matrix == null){
	    return 0;
	  }
	  
	  Integer rotateMax = getRotationCnt(angle);

	  // check angle if multiple of 90
	  if (rotateMax == null){
	    out.println("The angle [" + angle + "] must be multiple of 90");
	    return -1;
	  }

	  // check if matrix is not N X N
	  int rows = matrix.length;
	  // For each row, check if column length is same as total number of rows
	  for (int i = 0; i < rows; i++){
	    T[] thisRow = matrix[i];
	    if (thisRow == null){
	      out.println("The matrix does not have N X N size. It has null row at " + i + "th position");
	      return -1;
	    }
	    if (thisRow.length != rows){
	      out.println("The matrix does not have N X N size. It has " 
	          + rows + " rows but, row [" + (i+1) + "] has " + thisRow.length + " columns");
	      return -1;
	    }
	  }

	  // if number of rows are 0 or 1, then return now itself
	  // if angle is 0, then also return as is
	  if (rows <= 1 || angle == 0){
	    return 0;
	  }
	  
	  //	  out.println("\n Matrix before rotation is as follows: ");
	  //	  printMatrix(matrix);
	  //	  out.println("\n");
	  // String direction = (isClockwise)?"Clockwise" : "Anti-Clockwise";

	  int matSize = rows;
	  boolean isClockwise = (rotateMax < 0)? false : true;
	  rotateMax = (isClockwise)? rotateMax : (rotateMax * -1);
	  for (int rotateCnt = 0; rotateCnt < rotateMax; rotateCnt++){
	    // prn ("Rotating in " + direction + " by " + (rotateCnt + 1) * 90 + " degrees");
	    for (int slice = 0; slice < matSize/2; slice++){
	      // prn("Rotating slice " + slice);
	      // start with ith row
	      int i = slice;
	      int iThis, jThis;
        int iNext, jNext;
        T tmp1, tmp2;
	      for (int j = slice; j < (matSize - slice - 1); j++){
	        // go over each column 
	        // for each column in the row, 
	        //    we need to rotate four element either clockwise or anticlockwise
	        //    these four elements are relative wrt current matrix[i][j]	    		  
	        iThis = i;
	        jThis = j;
	        tmp1 = matrix[iThis][jThis];
	        for (int elemId = 0; elemId < 4; elemId++){
	          if (isClockwise){
	            // pick up next element as the one on right corner relative to each one
	            iNext = jThis;
	            jNext = matSize - iThis - 1;
	          } else {
	            // pick up next element as the one on left corner relative to each one 
	            jNext = iThis;
	            iNext = matSize - jThis - 1;
	          }
	          // push to-be-replaced one on temp var and replace with this one
	          tmp2 = matrix[iNext][jNext];
	          matrix[iNext][jNext] = tmp1;
	          tmp1 = tmp2;
	          iThis = iNext;
	          jThis = jNext;
	        }
	      }
	    }
	  }
//	  out.println("\n Matrix rotated " + direction + " in " + angle + " is as follows: ");
//	  printMatrix(matrix);
//	  out.println("\n");
	  return 0;
	}
}
