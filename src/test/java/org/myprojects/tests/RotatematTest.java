package org.myprojects.tests;

import static java.lang.System.out;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.Random;

import org.junit.Test;

import rotatematrix.RotateMatrix;

public class RotatematTest {


  /**
   * Print statement
   *
   * @param msg the msg
   */
  public void prn(String msg){
    System.out.println(msg);
  }

  /**
   * Prints the matrix.
   *
   * @param <T> the generic type
   * @param m the matrix
   */
  private static <T> void printMatrix(T[][] m){
    for (int i = 0; i < m.length; i++){
      for (int j = 0; j < m.length; j++){
        out.printf("%-10s", m[i][j].toString());
      }
      out.println();
    }
  }

  @Test
  public void rotateMatrixAngleCheck() {
    Integer matrix[][] = {{1,2,3},{4,5,6},{7,8,9}};

    assertNotEquals(RotateMatrix.<Integer>rotate(80, matrix), 0);
    assertEquals(RotateMatrix.<Integer>rotate(0, matrix), 0);
    assertNotEquals(RotateMatrix.<Integer>rotate(980, matrix), 0);
  }

  @Test
  public void rotateMatrixCheckSize() {
    Integer matrixFailures[][][] = {
        {{1,2,3},{4,5,6},{7,8}},
        {{1,2,3,4}, null, {4,5},{7}},
        {{1,2,3},{4,5,6},{7,8,9},{1,2,3,4}},
        {{1,2,3},{4,5,6}},
        {{1,2}}
    };

    Integer matrixPassed[][][] = {
        {{1,2,3},{4,5,6},{7,8,9}},
        {{1,2},{4,5}},
        {{1,2,3,4},{5,4,5,6},{7,5,8,9},{1,2,3,4}},
        {{1}},
        {}
    };

    prn("Running matrix invalid row-col combination tests ...");
    for (int i = 0; i < matrixFailures.length; i++){
      assertNotEquals("Failed to run size test on matrix " + i, RotateMatrix.<Integer>rotate(0, matrixFailures[i]), 0);
    }

    prn("Running matrix valid row-col combination tests ...");
    for (int i = 0; i < matrixPassed.length; i++){
      assertEquals("Failed to run size test on matrix " + i, RotateMatrix.<Integer>rotate(0, matrixPassed[i]), 0);
    }
  }

  @Test
  public void rotateCornerCases() {
    Integer matrixCornerCases[][][] = {
        {{1}},
        {},
        null
    };

    prn("Running corner case tests for size 0 and 1 ...");
    for (int i = 0; i < matrixCornerCases.length; i++){
      if (matrixCornerCases[i] != null){
        assertEquals("Failed to run corner case on matrix " + i + " with size " + matrixCornerCases[i].length
            , RotateMatrix.<Integer>rotate(0, matrixCornerCases[i]), 0);
      } else {
        assertEquals("Failed to run corner case on null matrix ", RotateMatrix.<Integer>rotate(0, matrixCornerCases[i]), 0);
      }
    }
  }

  @Test
  public void rotateNBasicCompare_3X3() {
    Integer matrix[][] = {{1,2,3},{4,5,6},{7,8,9}};
    Integer rot_p_90_Matrix[][] = {{7,4,1},{8,5,2},{9,6,3}};

    assertEquals(RotateMatrix.<Integer>rotate(90, matrix), 0);
    assertArrayEquals(rot_p_90_Matrix, matrix);
  }

  @Test
  public void rotateNBasicCompare_2X2() {
    Integer matrix[][] = {{1,2},{3,4}};
    Integer rot_p_90_Matrix[][] = {{3,1},{4,2}};

    assertEquals(RotateMatrix.<Integer>rotate(90, matrix), 0);

    assertArrayEquals(rot_p_90_Matrix, matrix);
  }

  @Test
  public void fullRotateNBasicCompare_5x5() {
    Integer matrixNegRotate[][][] = {
        {{1,2,3,4,5},{6,7,8,9,10},{11,12,13,14,15},{16,17,18,19,20},{21,22,23,24,25}},
        {{5,10,15,20,25},{4,9,14,19,24},{3,8,13,18,23},{2,7,12,17,22},{1,6,11,16,21}},
        {{25,24,23,22,21},{20,19,18,17,16},{15,14,13,12,11},{10,9,8,7,6},{5,4,3,2,1}},
        {{21,16,11,6,1},{22,17,12,7,2},{23,18,13,8,3},{24,19,14,9,4},{25,20,15,10,5}},
        {{1,2,3,4,5},{6,7,8,9,10},{11,12,13,14,15},{16,17,18,19,20},{21,22,23,24,25}}
    };

    Integer matrixPStd[][][] = {
        {{1,2,3,4,5},{6,7,8,9,10},{11,12,13,14,15},{16,17,18,19,20},{21,22,23,24,25}},
        {{1,2,3,4,5},{6,7,8,9,10},{11,12,13,14,15},{16,17,18,19,20},{21,22,23,24,25}},
        {{1,2,3,4,5},{6,7,8,9,10},{11,12,13,14,15},{16,17,18,19,20},{21,22,23,24,25}},
        {{1,2,3,4,5},{6,7,8,9,10},{11,12,13,14,15},{16,17,18,19,20},{21,22,23,24,25}},
        {{1,2,3,4,5},{6,7,8,9,10},{11,12,13,14,15},{16,17,18,19,20},{21,22,23,24,25}}
    };

    Integer matrixNStd[][][] = {
        {{1,2,3,4,5},{6,7,8,9,10},{11,12,13,14,15},{16,17,18,19,20},{21,22,23,24,25}},
        {{1,2,3,4,5},{6,7,8,9,10},{11,12,13,14,15},{16,17,18,19,20},{21,22,23,24,25}},
        {{1,2,3,4,5},{6,7,8,9,10},{11,12,13,14,15},{16,17,18,19,20},{21,22,23,24,25}},
        {{1,2,3,4,5},{6,7,8,9,10},{11,12,13,14,15},{16,17,18,19,20},{21,22,23,24,25}},
        {{1,2,3,4,5},{6,7,8,9,10},{11,12,13,14,15},{16,17,18,19,20},{21,22,23,24,25}}
    };

    Integer matrixPosRotate[][][] = {
        {{1,2,3,4,5},{6,7,8,9,10},{11,12,13,14,15},{16,17,18,19,20},{21,22,23,24,25}},
        {{21,16,11,6,1},{22,17,12,7,2},{23,18,13,8,3},{24,19,14,9,4},{25,20,15,10,5}},
        {{25,24,23,22,21},{20,19,18,17,16},{15,14,13,12,11},{10,9,8,7,6},{5,4,3,2,1}},
        {{5,10,15,20,25},{4,9,14,19,24},{3,8,13,18,23},{2,7,12,17,22},{1,6,11,16,21}},
        {{1,2,3,4,5},{6,7,8,9,10},{11,12,13,14,15},{16,17,18,19,20},{21,22,23,24,25}}
    };

    // positive rotation
    for (int i = 0; i < 5; i++){
      assertEquals("Internal Failure to rotate at angle " + (90*i), RotateMatrix.<Integer>rotate(90*i, matrixPStd[i]), 0);
      assertTrue("Failed to rotate at angle " + (90*i),Arrays.deepEquals(matrixPStd[i], matrixPosRotate[i]));
    }

    // negative rotation
    for (int i = 0; i < 5; i++){
      assertEquals("Internal Failure to rotate at angle " + (90*i), RotateMatrix.<Integer>rotate(-90*i, matrixNStd[i]), 0);
      assertTrue("Failed to rotate at angle " + (-90*i),Arrays.deepEquals(matrixNStd[i], matrixNegRotate[i]));
    }
  }

  @Test
  public void rotateNBasicCompare_NXN_RotateAntirotate() {
    Integer matrix1[][] = new Integer[23][23];
    Integer matrix2[][] = new Integer[23][23];
    Random rnd = new Random(12345);
    Integer val;
    for (int i = 0; i < 23; i++){
      for(int j = 0; j < 23; j++){
        val = rnd.nextInt(10000);
        matrix1[i][j] = val;
        matrix2[i][j] = val;
      }
    }

    for (int i = 0; i < 15; i++){
      assertEquals("Internal Failure to rotate at angle " + -(90*i), RotateMatrix.<Integer>rotate(-(90*i), matrix1),0);
      assertEquals("Internal Failure to rotate at angle " + (90*i), RotateMatrix.<Integer>rotate(+(90*i), matrix1),0);
      assertTrue("Failed to rotate for angle " + (90*i), Arrays.deepEquals(matrix1, matrix2));
    }
  }

  @Test
  public void rotateNBasicCompareStrings() {
    String matrix1[][] = {{"a", "b", "c", "d", "e"},{"f","g","h","i", "j"}
    ,{"k","l","m","n","o"},{"p","q","r","s","t"}, {"u","v","w","x","y"}};
    String matrix2[][] = {{"a", "b", "c", "d", "e"},{"f","g","h","i", "j"}
    ,{"k","l","m","n","o"},{"p","q","r","s","t"}, {"u","v","w","x","y"}};
    assertEquals(RotateMatrix.<String>rotate(-270, matrix1),0);
    assertEquals(RotateMatrix.<String>rotate(+270, matrix1),0);
    assertTrue("Failed to rotate strings",Arrays.deepEquals(matrix1, matrix2));
  }
}
